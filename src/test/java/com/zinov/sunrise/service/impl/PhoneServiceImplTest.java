package com.zinov.sunrise.service.impl;

import com.zinov.sunrise.config.TestApplicationConfiguration;
import com.zinov.sunrise.model.Phone;
import com.zinov.sunrise.repository.PhoneRepository;
import com.zinov.sunrise.repository.UserRepository;
import com.zinov.sunrise.service.PhoneService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplicationConfiguration.class)
public class PhoneServiceImplTest {

    @Autowired
    private PhoneService phoneService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private PhoneRepository phoneRepository;

    @Test
    public void delete() {
    }

    @Test
    public void save() {
        String userName = "test";
        String phoneNumber = "+79299603512";
        final Phone phone = phoneService.save(userName, phoneNumber);
        assertNotNull(phone);
    }

    @Test
    public void getAll() {
        assertEquals(phoneService.getAll(), 0);
    }

    @Test
    public void unloadToCSV() {
        assertFalse(phoneService.unloadToCSV().contains("error"));
    }
}
