package com.zinov.sunrise;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SunriseApplication {

    public static void main(String[] args) {

        System.out.println("Application is going to start. Please wait.");

        SpringApplication springApplication = new SpringApplication(SunriseApplication.class);
        springApplication.setBannerMode(Banner.Mode.OFF);
        springApplication.run(args);
    }

}
