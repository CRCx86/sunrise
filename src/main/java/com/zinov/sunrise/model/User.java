package com.zinov.sunrise.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "usr")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"id", "name"})
public class User implements Comparable<User> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;


    @Override
    public int compareTo(User o) {
        return this.id.compareTo(o.id);
    }
}
