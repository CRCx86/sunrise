package com.zinov.sunrise.repository;

import com.zinov.sunrise.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByName(String userName);

}
