package com.zinov.sunrise.repository;

import com.zinov.sunrise.model.Phone;
import com.zinov.sunrise.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneRepository extends JpaRepository<Phone, Long> {

    Phone findByNumber(String number);

    Phone findByUserAndNumber(User user, String number);
}
