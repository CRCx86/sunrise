package com.zinov.sunrise.service.impl;

import com.zinov.sunrise.model.User;
import com.zinov.sunrise.repository.UserRepository;
import com.zinov.sunrise.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User save(String userName) {

        User user = userRepository.findByName(userName);
        if (user == null) {
            user = new User();
            user.setName(userName);
        }
        return userRepository.save(user);
    }
}
