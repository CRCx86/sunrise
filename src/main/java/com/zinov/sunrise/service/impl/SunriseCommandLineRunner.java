package com.zinov.sunrise.service.impl;

import com.zinov.sunrise.controller.PhoneController;
import com.zinov.sunrise.controller.UserController;
import com.zinov.sunrise.exception.PhoneNotFoundException;
import com.zinov.sunrise.exception.ResourceConflictException;
import com.zinov.sunrise.exception.UserNotFoundException;
import com.zinov.sunrise.model.Phone;
import com.zinov.sunrise.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

@Component
public class SunriseCommandLineRunner implements CommandLineRunner {

    @Autowired
    UserController userController;

    @Autowired
    PhoneController phoneController;

    @Override
    public void run(String... args) throws Exception {

        try(Scanner in = new Scanner(System.in)) {

            String command = "";

            do {

                printCommandList();
                command = in.next();

                switch (command) {
                    case "1": {
                        createPhoneNumber(in);
                        break;
                    }
                    case "2": {
                        deletePhoneNumber(in);
                        break;
                    }
                    case "3": {
                        printPhoneNumbers();
                        break;
                    }
                    case "4": {
                        uploadPhoneNumbersToCSV();
                        break;
                    }
                    case "0": {
                        break;
                    }
                    default:
                        System.out.println("Something gones wrong! Please retry your operation.");
                }

            } while (!"0".equals(command));

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("To terminate application work please press Ctrl+C");
    }

    private void uploadPhoneNumbersToCSV() {
        System.out.println("CSV file has been uploaded to: " + phoneController.unloadToCSV());
    }

    private void printPhoneNumbers() {

        final Map<User, List<Phone>> phoneList = phoneController.getAll();
        if (phoneList.size() == 0) {
            System.out.println("Phone number list is empty");
        }

        phoneList.forEach((user, phones) -> {
            System.out.println(user.getName() + ":");
            System.out.println("phone list");
            phones.forEach(phone -> System.out.println(phone.getNumber()));
            System.out.println();
        });
    }

    private void deletePhoneNumber(Scanner in) {
        System.out.println("To delete number please enter user and phone number in next format: \"Aleksandr +79299603512 (89299603512)\"");
        String userName = in.next();
        String phoneNumber = in.next();
        try {
            phoneController.delete(userName, phoneNumber);
        } catch (UserNotFoundException e) {
            System.out.println("User not found!");
        } catch (PhoneNotFoundException e) {
            System.out.println("Phone not found!");
        } catch (ResourceConflictException e) {
            System.out.println("To delete number please enter user and phone number in next format: \"Aleksandr +79299603512 (89299603512)\"");
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
            System.out.println("To delete number please enter user and phone number in next format: \"Aleksandr +79299603512 (89299603512)\"");
        }
        System.out.println("phone number " + phoneNumber + " has been removed");
    }

    private void createPhoneNumber(Scanner in) {
        System.out.println("To add number please enter user and phone number in next format: \"Aleksandr +79299603512 (89299603512)\"");
        String userName = in.next();
        String phoneNumber = in.next();
        try {
            phoneController.create(userName, phoneNumber);
        } catch (UserNotFoundException e) {
            System.out.println("User not found! Do you want create new user with name " + userName + "? Y/n");
            String choice = in.next();
            if ("Y".equalsIgnoreCase(choice)) {
                final User user = userController.create(userName);
                if (user != null) {
                    System.out.println(String.format("User %s has been created", userName));
                    phoneController.create(userName, phoneNumber);
                }
            }
        } catch (ResourceConflictException e) {
            System.out.println(e.getMessage());
            System.out.println("To add number please enter user and phone number in next format: \"Aleksandr +79299603512 (89299603512)\"");
        } catch (RuntimeException e) {
            System.out.println("To add number please enter user and phone number in next format: \"Aleksandr +79299603512 (89299603512)\"");
        }
        System.out.println("phone number " + phoneNumber + " has been added");
    }

    private void printCommandList() {
        System.out.println("########### command list ###########");
        System.out.println("1 - add new phone number");
        System.out.println("2 - delete phone number");
        System.out.println("3 - print all user phone numbers");
        System.out.println("4 - upload to CSV");
        System.out.println("0 - to exit");
        System.out.println("######### end command list #########");
    }

}
