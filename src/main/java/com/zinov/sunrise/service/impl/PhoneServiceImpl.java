package com.zinov.sunrise.service.impl;

import com.zinov.sunrise.exception.PhoneNotFoundException;
import com.zinov.sunrise.exception.ResourceConflictException;
import com.zinov.sunrise.exception.UserNotFoundException;
import com.zinov.sunrise.model.Phone;
import com.zinov.sunrise.model.User;
import com.zinov.sunrise.repository.PhoneRepository;
import com.zinov.sunrise.repository.UserRepository;
import com.zinov.sunrise.service.PhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Service
public class PhoneServiceImpl implements PhoneService {

    private static final String CSV_FILENAME = "sunrise.csv";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PhoneRepository phoneRepository;

    @Override
    public void delete(String userName, String phoneNumber) {

        final User user = userRepository.findByName(userName);
        if (user == null)
            throw new UserNotFoundException(String.format("User %s not found", userName), 0l);

        final Phone number = phoneRepository.findByNumber(phoneNumber);
        if (number == null) {
            throw new PhoneNotFoundException(String.format("Number %s not found", phoneNumber), 0l);
        }

        phoneRepository.delete(number);

    }

    @Override
    public Phone save(String userName, String phoneNumber) {

        final User user = userRepository.findByName(userName);
        if (user == null)
            throw new UserNotFoundException(String.format("User %s not found", userName), 0l);

        Phone phone = phoneRepository.findByUserAndNumber(user, phoneNumber);
        if (phone != null)
            throw new ResourceConflictException("Phone number already exists", phone.getId());

        if (phoneNumber.length() < 11 || phoneNumber.length() > 12)
            throw new ResourceConflictException("phone number is too short or too large", 0l);

        phone = new Phone();
        phone.setUser(user);
        phone.setNumber(phoneNumber);
        return phoneRepository.save(phone);
    }

    @Override
    public Map<User, List<Phone>> getAll() {

        Map<User, List<Phone>> map = new TreeMap<>();

        phoneRepository.findAll().forEach(phone -> {
            List<Phone> phones = map.get(phone.getUser());
            if (phones != null) {
                phones.add(phone);
            } else {
                phones = new ArrayList<>();
                phones.add(phone);
            }
            map.put(phone.getUser(), phones);
        });

        return map;
    }

    @Override
    public String unloadToCSV() {

        String path = "upload";

        try {
            Files.createDirectories(Paths.get(path));
        } catch (IOException e) {
            return e.getMessage();
        }
        final File file = new File(path + File.separator + CSV_FILENAME);
        try (PrintWriter pw = new PrintWriter(file)) {
            prepareStrings()
                    .stream()
                    .map(this::convertToCSV)
                    .forEach(pw::println);
        } catch (FileNotFoundException e) {
            return e.getMessage();
        }

        return file.getAbsolutePath();
    }

    private String convertToCSV(String[] data) {
        return String.join(",", data);
    }

    private List<String[]> prepareStrings() {

        List<String[]> dataLines = new ArrayList<>();
        phoneRepository
                .findAll(new Sort(Sort.Direction.ASC, "user"))
                .forEach(phone -> {
                    dataLines.add(new String[]{phone.getUser().getName(), phone.getNumber()});
                });

        return dataLines;
    }

}
