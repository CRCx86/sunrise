package com.zinov.sunrise.service;

import com.zinov.sunrise.model.Phone;
import com.zinov.sunrise.model.User;

import java.util.List;
import java.util.Map;

public interface PhoneService {

    void delete(String userName, String phoneNumber);

    Phone save(String userName, String phoneNumber);

    Map<User, List<Phone>> getAll();

    String unloadToCSV();
}
