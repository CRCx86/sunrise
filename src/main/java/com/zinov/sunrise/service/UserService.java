package com.zinov.sunrise.service;

import com.zinov.sunrise.model.User;

public interface UserService {

    User save(String userName);

}
