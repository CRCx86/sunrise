package com.zinov.sunrise.controller;

import com.zinov.sunrise.model.User;
import com.zinov.sunrise.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    public User create(String userName) {
        return userService.save(userName);
    }
}
