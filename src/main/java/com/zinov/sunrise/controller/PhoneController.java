package com.zinov.sunrise.controller;

import com.zinov.sunrise.model.Phone;
import com.zinov.sunrise.model.User;
import com.zinov.sunrise.service.PhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Map;

@Controller
public class PhoneController {

    @Autowired
    PhoneService phoneService;

    public Map<User, List<Phone>> getAll() {
        return phoneService.getAll();
    }

    public Phone create(String userName, String phoneNumber) {
        return phoneService.save(userName, phoneNumber);
    }

    public void delete(String userName, String phoneNumber) {
        phoneService.delete(userName, phoneNumber);
    }

    public String unloadToCSV() {
        return phoneService.unloadToCSV();
    }
}
