package com.zinov.sunrise.exception;

public class UserNotFoundException extends ResourceConflictException {

    public UserNotFoundException(String message, Long resourceId) {
        super(message, resourceId);
    }
}
