package com.zinov.sunrise.exception;

public class PhoneNotFoundException  extends ResourceConflictException{

    public PhoneNotFoundException(String message, Long resourceId) {
        super(message, resourceId);
    }
}
