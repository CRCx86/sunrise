package com.zinov.sunrise.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResourceConflictException extends RuntimeException {

    private Long resourceId;

    public ResourceConflictException(String message, Long resourceId) {
        super(message);
        this.resourceId = resourceId;
    }
}
